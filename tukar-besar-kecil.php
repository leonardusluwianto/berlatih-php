<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    function tukar_besar_kecil($string){
        for($i = 0; $i < strlen($string); $i++) {
            // Cek huruf kecil
            if (($string[$i] >= 'a') && ($string[$i] <= 'z')) {
                $string[$i] = strtoupper($string[$i]);  // Konversi ke huruf kapital
            }
            // Cek huruf kapital
            else if (($string[$i] >= 'A') && ($string[$i] <= 'Z')) {
                $string[$i] = strtolower($string[$i]);  // Konversi ke huruf kecil
            }
        }
        return $string . "<br>";
    }

    // TEST CASES
    echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
    echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
    echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
    echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
    echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

    ?>
</body>
</html>